TODO LIST:
---------------------------------
RESEARCH: GITLAB for providing tracking, deployment functionality
---------------------------------
1. Refactor all DTOS
a) Rename the DTOs. Follow common naming pattern
b) Move DTO package in rest layer
c) Maybe move the mapper package in rest (optional)

2. Refactor Services/Repos
a) Return Optional from service and throw exception in Resource
b) Refactor exceptions that are thrown
c) Make necessary methods IgnoreCase

3. Implement functionality for sending emails
a) Maybe separate MS (needs research)

4. Write Unit and Integration TESTS !!!
a) refactor test Profile, use H2, Junit5

---------------------------------
5. UAA
a) move the user management in separate Service or use provider

6. Integrate with OIDC provider for UserIdentity(Keycloak, Auth0, Okkta or other)

================================
https://www.baeldung.com/category/series/
https://www.baeldung.com/spring-security-registration
